<?php foreach ($posts as $post_id => $post): ?>
  <div class="FacebookPost">
    <a class="FacebookPost-image" href="<?php print $user['url']; ?>" title="Visit <?php print $user['name']; ?>'s profile."><img src="<?php print $user['pic']; ?>"></a>

    <div class="FacebookPost-content">
      <span class="FacebookPost-message"><?php print $post['message']; ?></span>

      <a class="FacebookPost-more" href="<?php print $post['permalink']; ?>" title="Read the full post.">More</a>

      <span class="FacebookPost-date"><?php print $post['date']; ?></span>
    </div>
  </div>
<?php endforeach; ?>